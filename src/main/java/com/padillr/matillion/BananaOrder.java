package com.padillr.matillion;

public class BananaOrder extends FruitOrder
{
    @Override
    public String getSupplierName()
    {
        return "Banana Magic";
    }

    @Override
    public String getSupplierTelephone()
    {
        return "987-654-3210";
    }
}