package com.padillr.matillion;

public class AppleOrder extends FruitOrder
{
    @Override
    public String getSupplierName()
    {
        return "Apples R Us";
    }

    @Override
    public String getSupplierTelephone()
    {
        return "333-444-5555";
    }

    public String getSupplierEmail()
    {
        return "applesrus@somewhere.com";
    }
}