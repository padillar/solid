package com.padillr.matillion;

public class FruitDispatchNoteMaker
{
    public void makeDispatchNote(FruitOrder order, int numItems)
    {
        String noteTxt = "";

        if (order instanceof OrangeOrder)
        {
            noteTxt = createOrangeNote(order, numItems);
        }
        else if (order instanceof AppleOrder)
        {
            noteTxt = createAppleNote(order, numItems);
        }
        else if (order instanceof BananaOrder)
        {
            noteTxt = createBananaNote(order, numItems);
        }

        System.out.println(noteTxt);
    }

    private String createOrangeNote(FruitOrder orangeOrder, int numItems)
    {
        String noteTxt = numItems
                + " oranges supplied from source: "
                + orangeOrder.getSupplierName() + "\n";

        noteTxt += "Supplier can be contacted on: "
                + orangeOrder.getSupplierTelephone() + "\n\n";

        return noteTxt;
    }

    private String createAppleNote(FruitOrder fruitOrder, int numItems)
    {
        AppleOrder apple = (AppleOrder) fruitOrder;

        String noteTxt = numItems + " apples supplied from source: "
                + apple.getSupplierName() + "\n";

        noteTxt += "Supplier can be contacted on: "
                + apple.getSupplierTelephone() + "\n\n";

        noteTxt += "or via email at: "
                + apple.getSupplierEmail() + "\n\n";

        return noteTxt;
    }

    private String createBananaNote(FruitOrder bananaOrder, int numItems)
    {
        String noteTxt = "Bananas: " + numItems
                + " of, supplied by " + bananaOrder.getSupplierName() + "\n\n";

        return noteTxt;
    }
}