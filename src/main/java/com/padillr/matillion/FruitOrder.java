package com.padillr.matillion;

public abstract class FruitOrder
{
    public abstract String getSupplierName();

    public abstract String getSupplierTelephone();
}