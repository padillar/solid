package com.padillr.matillion;

public class Runner
{
    public static void main(String[] args)
    {
        FruitDispatchNoteMaker noteMaker = new FruitDispatchNoteMaker();

        AppleOrder appleOrder = new AppleOrder();

        noteMaker.makeDispatchNote(appleOrder, 100);
    }
}