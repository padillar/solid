package com.padillr.matillion;

public class OrangeOrder extends FruitOrder
{
    @Override
    public String getSupplierName()
    {
        return "Orangetastic Inc.";
    }

    @Override
    public String getSupplierTelephone()
    {
        return "123-446-7890";
    }
}